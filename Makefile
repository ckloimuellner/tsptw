include mhlib/makefile.common

#CXXFLAGS =	-O4 -g -Wall -fmessage-length=0

OBJS =		TSPTW.o Main.o Instance.o

LIBS =		-lmh -lgurobi_c++ -lgurobi65

INCLUDE_PATHS =	-I$(GUROBI_HOME)/include/

LIBRARY_PATHS =	-L$(GUROBI_HOME)/lib/  

TARGET =	TSPTW

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
