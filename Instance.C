/*
 * Instance.C
 *
 *  Created on: Dec 11, 2015
 *      Author: kloimuellner
 */

#include "Instance.h"

using namespace std;

Instance::Instance() {
	// TODO Auto-generated constructor stub

}

Instance::~Instance() {
	// TODO Auto-generated destructor stub
}

void Instance::readFile(string filename) {
	// input file stream
	ifstream is(filename.c_str(), ios::in);

	if(is.is_open()) {
		is >> numNodes;

		t = new int[numNodes];
		for(int i = 0; i < numNodes; i++) {
			t[i] = new int[numNodes];

			for(int j = 0; j < numNodes; j++)
				is >> t[i][j];
		}

		for(int i = 0; i < numNodes; i++) {
			nodes.push_back(new Node());
			is >> nodes[i]->e;
			is >> nodes[i]->l;
		}

		for(const auto &u : nodes)
			for(const auto &v : nodes)
				if(u != v)
					edges.push_back(new Edge(u, v));
	}
}
