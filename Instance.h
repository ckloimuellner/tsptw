/*
 * Instance.h
 *
 *  Created on: Dec 11, 2015
 *      Author: kloimuellner
 */

#ifndef INSTANCE_H_
#define INSTANCE_H_


class Instance {
public:
	Instance();
	virtual ~Instance();

	void readFile(std::string filename);

	struct Edge;
	struct Node {
		int id;
		/** earliest arrival */
		int e;
		/** latest arrival */
		int l;

		std::vector<Edge *> outoing;
		std::vector<Edge *> ingoing;
	};

	struct Edge {
		Node *u;
		Node *v;

		Edge(Node *_u, Node *_v) : u(_u), v(_v) {}
	};

	/**
	 * u = node id of u
	 * v = node id of v
	 */
	int getTravelTime(int u, int v);
private:
	int **t;
	int numNodes;
	std::vector<Node *> nodes;
	std::vector<Edge *> edges;
};

#endif /* INSTANCE_H_ */
