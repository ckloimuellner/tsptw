/*
 * TSPTW.h
 *
 *  Created on: Dec 11, 2015
 *      Author: kloimuellner
 */

#ifndef TSPTW_H_
#define TSPTW_H_

#include "gurobi_c++.h"

class TSPTW {
public:
	TSPTW();
	virtual ~TSPTW();

	void perform();
private:
	GRBEnv env;
	GRBModel model;
	/**
	 * \defgroup Decision variables for the routing mixed integer linear program
	 * @{
	 */
	/// binary variable deciding whether an edge between two nodes is used in the solution
	/// or not
	GRBVar **y;
	/// real variable for determining the arrival time at each node of the given problem
	/// instance
	GRBVar *a;

	GRBVar *twait;
	/**@}*/

	void initializeVariables();
	void printSolution();
};


#endif /* TSPTW_H_ */
