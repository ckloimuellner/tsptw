/*! \file mh_genea.h 
	\brief A generational EA. */

#ifndef MH_GENEA_H
#define MH_GENEA_H

#include "mh_advbase.h"
#include "mh_param.h"


/** \ingroup param
	Should elitism be used?.
	True if the best solution should be kept, false if not. */
extern bool_param elit;

/** A generational EA.
	During each generation, all solutions are replaced by new ones
	generated by means of variation operators (crossover and mutation). */
class generationalEA : public mh_advbase
{
public:
	/** The constructor.
		An initialized population already containing chromosomes 
		must be given. Note that the population is NOT owned by the 
		EA and will not be deleted by its destructor. */
	generationalEA(pop_base &p, const pstring &pg=(pstring)(""));
	/** Another constructor.
		Creates an empty EA that can only be used as a template. */
	generationalEA(const pstring &pg=(pstring)("")) : mh_advbase(pg) {};
	/** The destructor. */
	~generationalEA();
	/** Create new steadyStateGA.
		Returns a pointer to a new steadyStateEA. */
	mh_advbase *clone(pop_base &p,const pstring &pg=(pstring)(""))
	    { return new generationalEA(p,pg); }
	/** Performs a single generation. */
	void performIteration();
	/** The selection function.
		Calls a concrete selection technique and returns the index
		of the selected chromosome in the population. */
	virtual int select()
		{ nSelections++; return tournamentSelection(); }

protected:
	/** The crossover and mutation function.
		Creates a new generation from the selected chromosomes by using
		crossover and mutation. */
	virtual void createNextGeneration();
	
	int *selectedChroms;          // indizes of selected chromosomes
	mh_solution **nextGeneration;  // used to build the next generation
};

#endif //MH_GENGA_H
